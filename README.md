#Program Automation Report
**Automation Report**

**Deskripsi Program: **
Program ini untuk keperluan parsing file show command router cisco **.txt** ke **.docx**, dengan mengambil bagian yang diinginkan dari file show command router cisco **.txt** dan di parsing ke file .docx menggunakan regex

**cara pakai:**
tambahkan 2 folder kosong **input_log** dan **output** ke directory automation_report/
masukkan file show command router cisco ke folder **input_log**
dan jalankan **manual.py** 
hasilnya bisa dilihat di folder **output**

**Show Command Router Cisco yang bisa diambil datanya sesuai template docx**
show running-config
show version
show cdp neighbors
show cdp neighbors detail
show ip router
show ip interface brief
show inventory


