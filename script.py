from csv import Sniffer
import re
import os
from tokenize import group
# from turtle import pd



def pertamina():
            # print(tambah)
            

                # print (dir)
            for file in os.listdir('input_log/'):
                print (file)
                    
                with open('input_log/' + file) as f:
                    hasil = f.read()
                    # print(hasil)
                    #info perangkat

                    tambah = []
                    
                    get_sh_running=r'^(.\.*\w+\S+#show running-config.*^.\.*\w+\S+#show version)'
                    get_sh_version=r'^(.\.*\w+\S+#show version.*^.\.*\w+\S+#show cdp neighbors)'
                    get_sh_cdp_neighbors=r'^(.\.*\w+\S+#show cdp neighbors.*^.\.*\w+\S+#show cdp neighbors detail)'
                    get_sh_cdp_neighbors_detail=r'^(.\.*\w+\S+#show cdp neighbors detail.*^.\.*\w+\S+#show ip interface brief)'
                    get_sh_ip_interface_brief=r'^(.\.*\w+\S+#show ip interface brief.*^.\.*\w+\S+#show ip route)'
                    get_sh_ip_route=r'^(.\.*\w+\S+#show ip route.*^.\.*\w+\S+#show inventory)'
                    get_sh_inventory=r'^(.\.*\w+\S+#show inventory.*)'
                    # get_lok_perangkat =r'^.*Site\s+:\s+(.*)'
                    


                    sh_running=re.search(get_sh_running, hasil,re.MULTILINE | re.DOTALL).group(0)
                    sh_version=re.search(get_sh_version, hasil,re.MULTILINE | re.DOTALL).group(0)
                    sh_cdp_neighbors=re.search(get_sh_cdp_neighbors, hasil,re.MULTILINE | re.DOTALL).group(0)
                    sh_cdp_neighbors_detail=re.search(get_sh_cdp_neighbors_detail, hasil,re.MULTILINE | re.DOTALL).group(0)
                    sh_ip_interface_brief=re.search(get_sh_ip_interface_brief, hasil,re.MULTILINE | re.DOTALL).group(0)
                    sh_ip_route=re.search(get_sh_ip_route, hasil,re.MULTILINE | re.DOTALL).group(0)
                    sh_inventory=re.search(get_sh_inventory, hasil,re.MULTILINE | re.DOTALL).group(0)

                    lok_perangkat = re.findall('^.*Site\s+:\s+(.*)', hasil, re.MULTILINE )
                    lok_perangkat = lok_perangkat[0]
                    # rull = re.findall('^.*Site\s+:\s+(.*)', hasil, re.MULTILINE )
                    # rull= rull[0]

                    hostname = re.findall('^.*hostname\s+(\S+)', hasil, re.MULTILINE)
                    hostname = hostname[0]

                    ip_address = re.findall('^\S+Ethernet0/0\s+(\S+).*', hasil, re.MULTILINE)
                    ip_address = ip_address[0]


                    tambah.append({
                        "nama file": file,
                        "show running": sh_running,
                        "show version": sh_version,
                        "show cdp neighbors": sh_cdp_neighbors,
                        "show cdp neighbors detail": sh_cdp_neighbors_detail,
                        "show ip interface brief": sh_ip_interface_brief,
                        "show ip route": sh_ip_route,
                        "show inventory": sh_inventory,

                        })
                    
                    
                    return (tambah)

                    

# print(pertamina())

def info_perangkat():

     tambah1 = {}

     for file in os.listdir('input_log/') :
                with open('input_log/' + file) as f:
                    hasil = f.read()

                    lok_perangkat = re.findall('^.*Site\s+:\s+(.*)', hasil, re.MULTILINE )
                    lok_perangkat = lok_perangkat[0]

                    hostname = re.findall('^.*hostname\s+(\S+)', hasil, re.MULTILINE)
                    hostname = hostname[0]

                    ip_address = re.findall('^\S+Ethernet0/0\s+(\S+).*', hasil, re.MULTILINE)
                    ip_address = ip_address[0]


                    

                    tambah1.update(
                        {
                        'i.': f'Lokasi Perangkat\t\t\t: {lok_perangkat}',
                        'ii.': f'Alamat Perangkat\t\t\t: {lok_perangkat}',
                        'iii.': f'Router Untuk Layanan Link\t\t: PT. Telkom Indonesia (Persero) Tbk.',
                        'iv.': f'Hostname\t\t\t\t: {hostname}',
                        'v.': f'IP Address\t\t\t\t: {ip_address}',
                        'vi.': f'Terminating\t\t\t\t: -',
                        'vii.': f'Lokasi Terminating\t\t\t: -',

                        

                        }
                    )

                    # tambah2.update({
                    #     f'Device Type\t\t\t: {device_type}': f'Serial Number\t\t\t: {serial_number}'

                    # })
                    return (tambah1)
                


# info = info_perangkat()
# for i in info:
#     print(i, info[i])
# print(info_perangkat())


def table_router():
     tambah2 = {}

     for file in os.listdir('input_log/') :
                with open('input_log/' + file) as f:
                    hasil = f.read()

                    serial_number = re.findall('^PID: .*,.*,.*SN: (.*)', hasil, re.MULTILINE)
                    serial_number = serial_number[0]

                    device_type = re.findall('^PID:\s+(\S+).*', hasil, re.MULTILINE)
                    device_type = device_type[0]

                    # device_type1 = re.findall('^PID:\s+(\S+).*', hasil, re.MULTILINE)
                    # device_type1 = device_type1[1:]

                    # serial_number1 = re.findall('^PID: .*,.*,.*SN: (.*)', hasil, re.MULTILINE)
                    # serial_number1 = serial_number1[1:]

                    tambah2.update({
                        f'Device Type\t\t: {device_type}': f'Serial Number\t\t: {serial_number}',
                        # f'Device Type\t\t: {device_type1}': f'Serial Number\t\t: {serial_number1}'

                    })
                
                return (tambah2)


# router = table_router()
# print (router)

# for j in router:
#     print(j, router[j])
# print(info_perangkat())


def modul_pendukung():
     tambah = {}
    #  print(tambah)
     for file in os.listdir('input_log/') :
                with open('input_log/' + file) as f:
                    hasil = f.read()

                    serial_number = re.findall('^PID: .*,.*,.*SN: (.*)', hasil, re.MULTILINE)
                    serial_number = serial_number[1:]
                    if len(serial_number) == 0:
                         serial_number = '-'
            

                    device_type = re.findall('^PID:\s+(\S+).*', hasil, re.MULTILINE)
                    device_type = device_type[1:]
                    pdevice1 = len(device_type)
                    # print (pdevice1)
                    # for d in device_type:
                    #     print(d)
                    # for k in range(pdevice1):
                    #     for d in device_type:

                            
                        
                    #         print(k, d)
                        # print(k, d)
                        # d += d
                    
                  

                    
                    
                    # for pid in device_type:
                    tambah.update({
                #         f"{device_type}" : f"{serial_number}",
                        # f"device type : {device_type}" : f"serial number : {serial_number}" 
                        'device type' :   device_type,
                        'serial number' : serial_number,



                    })


                # for device in tambah:
                #     print(tambah[device])
                
                # for sn in tambah:
                #     print(tambah[sn])



                # device = (tambah['device type'])
                # print (device)
                

                return (tambah)

modul = modul_pendukung()
# device = (modul['device type'])
# # print(device)

# for i in device:
#     print (i)

# print(modul['serial number'])

print(modul)

# for m, y in modul.items():
#     print(modul[m], [y])

