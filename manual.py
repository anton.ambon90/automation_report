import re
import os
# from turtle import width

from docx import Document
from docx.shared import Pt
from docx.shared import Inches
from docx.shared import Cm
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.enum.text import WD_LINE_SPACING
from docx.enum.table import WD_ROW_HEIGHT_RULE
from docx.enum.table import WD_TABLE_ALIGNMENT
from docx.enum.table import WD_ALIGN_VERTICAL

import docx

from docx.enum.style import WD_STYLE_TYPE
from docx.enum.style import WD_STYLE
from PIL import Image




for file in os.listdir('input_log/'):
                    
    with open('input_log/' + file) as f:
        hasil = f.read()
        
        #get data show command by Regex
        get_sh_running=r'^(.\.*\w+\S+#show running-config.*^.\.*\w+\S+#show logging)'
        get_sh_cdp_neighbors_detail=r'^(.\.*\w+\S+#show cdp neighbors detail.*^.\.*\w+\S+#term leng 24)'
        get_sh_cdp_neighbors=r'^(.\.*\w+\S+#show cdp neighbors.*^.\.*\w+\S+#show cdp neighbors detail)'
        get_sh_ip_interface_brief=r'^(.\.*\w+\S+#show ip interface brief.*^.\.*\w+\S+#show inventory)'
        get_sh_ip_route=r'^(.\.*\w+\S+#show ip route.*^.\.*\w+\S+#show ip route summary)'
        get_sh_inventory=r'^(.\.*\w+\S+#show inventory.*^.\.*\w+\S+#show diag)'
        get_sh_version=r'^(.\.*\w+\S+#show version.*^.\.*\w+\S+#show ip interface brief)' 
        
        



        sh_running=re.search(get_sh_running, hasil,re.MULTILINE | re.DOTALL).group(0)
        sh_running =sh_running.replace('show logging', '!')
        sh_version=re.search(get_sh_version, hasil,re.MULTILINE | re.DOTALL).group(0)
        sh_version =sh_version.replace('show ip interface brief', '!')
        sh_cdp_neighbors_detail=re.search(get_sh_cdp_neighbors_detail, hasil,re.MULTILINE | re.DOTALL).group(0)
        sh_cdp_neighbors_detail =sh_cdp_neighbors_detail.replace('term leng 24', '!')
        sh_cdp_neighbors=re.search(get_sh_cdp_neighbors, hasil,re.MULTILINE | re.DOTALL).group(0)
        sh_cdp_neighbors =sh_cdp_neighbors.replace('show cdp neighbors detail', '!')
        sh_ip_interface_brief=re.search(get_sh_ip_interface_brief, hasil,re.MULTILINE | re.DOTALL).group(0)
        sh_ip_interface_brief =sh_ip_interface_brief.replace('show inventory', '!')
        sh_ip_route=re.search(get_sh_ip_route, hasil,re.MULTILINE | re.DOTALL).group(0)
        sh_ip_route =sh_ip_route.replace('show ip route summary', '!')
        sh_inventory=re.search(get_sh_inventory, hasil,re.MULTILINE | re.DOTALL).group(0)
        sh_inventory =sh_inventory.replace('show diag', '!')

        
        
        
        gab_show = (sh_running, sh_version, sh_cdp_neighbors, sh_cdp_neighbors_detail, sh_ip_interface_brief, sh_ip_route, sh_inventory)
        
        title_show_command = (
           'Show Runnig-Config', 'Show Version', 'Show Cdp Neighbors', 'Show Cdp Neighbors Detail', 'Show Ip Interface Brief', 'Show Ip Route' , 'Show Inventory', 
        )
        
        
                  
        # using document docx module
        report_template = open('template_pertamina.docx', 'rb')
        document = Document(report_template)
        document1 = Document(report_template)

        
        
        #Create Document docx        
        # document = Document()
        sections = document.sections
        for section in sections:
            section.top_margin = Cm(2)
            section.bottom_margin = Cm(2)
            section.left_margin = Cm(2)
            section.right_margin = Cm(2)
        
        
        #get data lokasi perangkat by Regex
        lok_perangkat = re.findall('^.*Site\s+:\s+(.*)', hasil, re.MULTILINE ) or re.findall('^.*\s+Lokasi\s+Router\s+:\s+(\D+)^', hasil, re.MULTILINE)
        if len(lok_perangkat) == 0:
            lok_perangkat = '-'
        else:    
            lok_perangkat = lok_perangkat[0]

        hostname = re.findall('^.*hostname\s+(\S+)', hasil, re.MULTILINE)
        hostname = hostname[0]

        ip_address = re.findall('^\S+Ethernet0/0\s+(\S+).*', hasil, re.MULTILINE) or re.findall('^\S+Ethernet0/0/0\s+(\S+).*', hasil, re.MULTILINE)
        ip_address = ip_address[0]

   

        info = (
            {
                'i.': f'Lokasi Perangkat\t\t\t: {lok_perangkat}',
                'ii.': f'Alamat Perangkat\t\t\t: {lok_perangkat}',
                'iii.': f'Router Untuk Layanan Link\t\t: PT. Telkom Indonesia (Persero) Tbk.',
                'iv.': f'Hostname\t\t\t\t: {hostname}',
                'v.': f'IP Address\t\t\t\t: {ip_address}',
                'vi.': f'Terminating\t\t\t\t: -',
                'vii.': f'Lokasi Terminating\t\t\t: -',

                        

            }
            )
        
               
        # add title informasi perangkat
        p = document.add_paragraph()
        # styles = doc_info_perangkat.styles
        
        # perangkat = doc_info_perangkat.add_paragraph()
        
        # p.add_run('Informasi Perangkat').bold = True
        # style1 = document.styles['Normal'].font
        # style1.name = "Arial"
        # style1.size = Pt(11)
        # p.paragraph_format.space_after = Pt(0)
        p = document.add_paragraph()
        title = p.add_run('Informasi Perangkat')
        title.bold = True
        title.font.name = 'Arial'
        title.font.size = docx.shared.Pt(11)

            
        
        # Creating a table object informasi perangkat
        table_perangkat = document.add_table(rows=0, cols=2, style='Table Grid') 
        for k in info:
            row = table_perangkat.add_row().cells
            row[0].text = str(k)
            row[0].allow_autofit = True
            row_perangkat = row[0].paragraphs[0].runs[0].font
            row_perangkat.name = 'Arial'
            row_perangkat.size = Pt(11)
            row[1].text = str(info[k])
            row_perangkat1 = row[1].paragraphs[0].runs[0].font
            row_perangkat1.name = 'Arial'
            row_perangkat1.size = Pt(11)

            
            
            # Lebar Cols
            row[0].width = Cm(1.12)
            row[1].width = Cm(16.50)

        
        p = document.add_paragraph()
        p.paragraph_format.space_before = Pt(12)

        title = p.add_run('Router')
        title.bold = True
        title.font.name ='Arial'
        title.font.size = docx.shared.Pt(11)
        title = p.paragraph_format.space_after = Pt(0)
        

        #get Router by Regex
        serial_number = re.findall('^PID: .*,.*,.*SN: (.*)', hasil, re.MULTILINE)
        serial_number = serial_number[0]

        device_type = re.findall('^PID:\s+(\S+).*', hasil, re.MULTILINE)
        device_type = device_type[0]
        
        router = ({
                    f'Device Type\t\t: {device_type}': f'Serial Number\t\t: {serial_number}',
                    # f'Device Type\t\t: {device_type1}': f'Serial Number\t\t: {serial_number1}'

                    })

        # Creating a table object Router
        table = document.add_table(rows=0, cols=2, style='Table Grid') 
        for i in router:
            row = table.add_row().cells
            row[0].text = str(i)
            row[0].allow_autofit = True
            row0 = row[0].paragraphs[0].runs[0].font
            row0.name = 'Arial'
            row0.size = Pt(11)

            row[1].text = str(router[i])
            row1 = row[1].paragraphs[0].runs[0].font
            row1.name = 'Arial'
            row1.size = Pt(11)
        
            # Lebar Cols
            row[0].width = Cm(8.81)
            row[1].width = Cm(8.81)
            # row[2].width = Cm(10)
            
        p = document.add_paragraph('')
        p.paragraph_format.space_before = Pt(12)
        
        #get data Modul Pendukung by regex
        serial_number = re.findall('^PID: .*,.*,.*SN: (.*)', hasil, re.MULTILINE)
        serial_number = serial_number[1:]
        # if len(serial_number) == 0:
        #     serial_number = '-'
            

        device_type = re.findall('^PID:\s+(\S+).*', hasil, re.MULTILINE)
        device_type = device_type[1:] 
        
        
        modul =({
                    'device type' :   device_type,
                    'serial number' : serial_number,
                })
        
        device = (modul['device type'])
        serialnumber = (modul['serial number'])
        
         # add title object Modul Pendukung
        
        title = p.add_run('Modul Pendukung ')
        title1 = p.add_run('(isi jika ada instalasi modul. jika tidak ada, berikan informasi “tidak ada instalasi modul” pada tabel)')
        p.alignment = 3 # for left, 1 for center, 2 right, 3 justify ...

        # title = p.paragraph_format.space_after = Pt(0)
        title.bold = True
        title.font.name ='Arial'
        title.font.size = docx.shared.Pt(11)
        title1.italic = True
        title1.font.name ='Arial'
        title1.font.size = docx.shared.Pt(11)
        # title = p.paragraph_format.space_after = Pt(0)

        # Creating a table object Modul Pendukung
        table = document.add_table(rows=0, cols=3, style='Table Grid') 
        number = ['i', 'ii', 'iii', 'iv', 'v', 'vi']
        for (n, d, s)in zip(number, device, serialnumber) :
            row = table.add_row().cells
            row[0].text = str(n)
            run0 = row[0].paragraphs[0].runs[0].font
            run0.name = 'Arial'
            run0.size = Pt(11)
            row[1].text = str(d)
            run0 = row[1].paragraphs[0].runs[0].font
            run0.name = 'Arial'
            run0.size = Pt(11)
            row[2].text = str(f"Serial Number : {s}")
            run0 = row[2].paragraphs[0].runs[0].font
            run0.name = 'Arial'
            run0.size = Pt(11)
        
            # Lebar Cols
            row[0].width = Cm(1.12)
            row[1].width = Cm(8.25)
            row[2].width = Cm(8.25)

        p = document.add_paragraph('')
        p.paragraph_format.space_before = Pt(12)  
        
        
        #Creating a title informasi Teknis Perangkat
        title = p.add_run('Informasi Teknis Perangkat')
        title.bold = True
        # p.paragraph_format.space_after = Pt(0)
        title.font.name ='Tahoma'
        title.font.size = Pt(11)
        
        #Create Table Show Command
        for sh, isi_show in zip(title_show_command, gab_show):
            table_show_command = document.add_table(rows=1, cols=1, style='Table Grid')
            hdr_cells = table_show_command.rows[0].cells
            hdr_cells[0].text = str(sh)
            hdr_cells[0].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.CENTER
            run0 = hdr_cells[0].paragraphs[0].runs[0].font
            run0.name = 'Tahoma'
            run0.size = Pt(11)
            run0.bold = True
            # for k in sh_cdp_neighbors:
            # for gt in gab_show :    
            row_cells = table_show_command.add_row().cells
            row_cells[0].text = isi_show
            row0 = row_cells[0].paragraphs[0].runs[0].font
            row0.name = 'Tahoma'
            row0.size = Pt(10)
            row0.italic = True
            
        
        # add table Ping Test IP Router & IP Backhaul
        p = document.add_paragraph('')
        # p.style = document.styles['No Spacing']

        ptirib = document.add_table(rows=1, cols=1, style='Table Grid')
        hdr_cells = ptirib.rows[0].cells
        hdr_cells[0].text = 'Ping Test IP Router & IP Backhaul'
        hdr_cells[0].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.CENTER
        run0 = hdr_cells[0].paragraphs[0].runs[0].font
        run0.bold = True
        run0.name = 'Tahoma'
        run0.size = Pt(11)
        hdr_cells[0].width = Cm(17.37)
        row_cells = ptirib.add_row().cells
        row_cells[0].text = str('Wajib:\n\t1. Tampilkan versi gambar/ capture full layar/ print screen\n\t2. Tampilkan versi text\n\t3. Tampilkan kalimat perintahnya\n\t4. Tampilkan perintah tanggal/ show clock\n\t5. Tampilkan informasi yang muncul jika terjadi gagal perintah\n\t6. Tampilkan Before & After jika relokasi/ pergantian perangkat (replacement)\n(hapus panduan kalimat miring ketika akan diisi informasi)')
        row0 = row_cells[0].paragraphs[0].runs[0].font
        row0.italic = True
        row0.name = 'Tahoma'
        row0.size = Pt(10)
        row_cells[0].width =Cm(17.37)
        
        
        # p = document.add_paragraph('')
        p = document.add_paragraph()
        p.paragraph_format.space_before = Pt(12)

        # Adding list of style name 'List Number'
        # document.add_heading('Style: List Number', 3)
        # Adding points to the list named 'List Number'
        

        title = p.add_run('Gambar Perangkat')
        title.bold = True
        title.font.name = 'Tahoma'
        title.font.size = Pt(11)
        # bold = p.add_run('Gambar Perangkat').bold = True

        p.paragraph_format.space_after = Pt(0)
        p = document.add_paragraph()
        runner = p.add_run('Isi jika instalasi baru/ relokasi/ pergantian perangkat (replacement)/ dismantle (cabut layanan)')
        runner.italic = True
        runner.font.name = 'Tahoma'
        runner.font.size = Pt(10)
        # p.style = document.styles['List Number.']
        # latent_styles = document.styles.latent_styles
        p.style = document.styles['List Number']

        
        # latent_style = latent_styles.add_latent_style('List Number')
       
        # latent_style.hidden = False
        # latent_style.priority = 2
        # latent_style.quick_style = True
        
        p = document.add_paragraph()
        runner = p.add_run('Isi dengan keterangan ')
        runner.italic = True
        runner.font.name = 'Tahoma'
        runner.font.size = Pt(10)
        runner = p.add_run('Aktivitas backup data tanggal … ')
        runner.bold = True
        runner.font.name = 'Tahoma'
        runner.font.size = Pt(10)
        runner.italic = True
        runner = p.add_run('jika bukan instalasi baru/ relokasi/ pergantian perangkat (replacement) dan hapus tabel dibawah ini')
        runner.italic = True
        p.style = document.styles['List Number']
        p.alignment = 3 # for left, 1 for center, 2 right, 3 justify ...

        runner.font.name = 'Tahoma'
        runner.font.size = Pt(10)

        p.paragraph_format.space_after = Pt(0)

        # add table Gambar Lokasi/ Site/ Kantor
        p = document.add_paragraph('')


        ptirib = document.add_table(rows=1, cols=1, style='Table Grid')
        hdr_cells = ptirib.rows[0].cells
        hdr_cells[0].text = 'Gambar Lokasi/ Site/ Kantor'
        hdr_cells[0].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.CENTER
        run0 = hdr_cells[0].paragraphs[0].runs[0]
        run0.bold = True
        run0 = hdr_cells[0].paragraphs[0].runs[0].font
        run0.name = 'Tahoma'
        run0.size = Pt(11)
        hdr_cells[0].width = Cm(17.37)
        row_cells = ptirib.add_row().cells
        row_cells[0].text = str('\tFoto Before & After instalasi/ implementasi dg aplikasi kamera GPS\n\t(hapus panduan kalimat miring ketika akan diisi informasi)')
        run0 = row_cells[0].paragraphs[0].runs[0].font
        run0.name = 'Tahoma'
        run0.size = Pt(10)
        run0.italic = True
        row_cells[0].width = Cm(17.37)
        p = document.add_paragraph('')
        
        
        #gambar lokasi ruangan perangkat
        ptirib = document.add_table(rows=1, cols=1, style='Table Grid')
        hdr_cells = ptirib.rows[0].cells
        hdr_cells[0].text = 'Gambar Lokasi/ Ruangan Perangkat'
        hdr_cells[0].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.CENTER
        run0 = hdr_cells[0].paragraphs[0].runs[0]
        run0.bold = True
        run0 = hdr_cells[0].paragraphs[0].runs[0].font
        run0.name = 'Tahoma'
        run0.size = Pt(11)
        hdr_cells[0].width = Cm(17.37)
        row_cells = ptirib.add_row().cells
        row_cells[0].text = str('\tFoto Before & After instalasi/ implementasi dg aplikasi kamera GPS\n\t(hapus panduan kalimat miring ketika akan diisi informasi)')
        run0 = row_cells[0].paragraphs[0].runs[0].font
        run0.name = 'Tahoma'
        run0.size = Pt(10)
        run0.italic = True
        row_cells[0].width = Cm(17.37)
        p = document.add_paragraph('')
        
        #Gambar Rak/ Wadah Perangkat, AC, UPS
        ptirib = document.add_table(rows=1, cols=1, style='Table Grid')
        hdr_cells = ptirib.rows[0].cells
        hdr_cells[0].text = 'Gambar Rak/ Wadah Perangkat, AC, UPS'
        hdr_cells[0].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.CENTER
        run0 = hdr_cells[0].paragraphs[0].runs[0]
        run0.bold = True
        run0 = hdr_cells[0].paragraphs[0].runs[0].font
        run0.name = 'Tahoma'
        run0.size = Pt(11)
        hdr_cells[0].width = Cm(17.37)
        row_cells = ptirib.add_row().cells
        row_cells[0].text = str('\tFoto Before & After instalasi/ implementasi dg aplikasi kamera GPS\n\t(hapus panduan kalimat miring ketika akan diisi informasi)')
        run0 = row_cells[0].paragraphs[0].runs[0].font
        run0.name = 'Tahoma'
        run0.size = Pt(10)
        run0.italic = True
        row_cells[0].width = Cm(17.37)
        p = document.add_paragraph('')
        
        
        #Gambar Kabel (Power & Konektivitas) & Indikator
        ptirib = document.add_table(rows=1, cols=1, style='Table Grid')
        hdr_cells = ptirib.rows[0].cells
        hdr_cells[0].text = 'Gambar Kabel (Power & Konektivitas) & Indikator'
        hdr_cells[0].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.CENTER
        run0 = hdr_cells[0].paragraphs[0].runs[0]
        run0.bold = True
        run0 = hdr_cells[0].paragraphs[0].runs[0].font
        run0.name = 'Tahoma'
        run0.size = Pt(11)
        hdr_cells[0].width = Cm(17.37)
        row_cells = ptirib.add_row().cells
        row_cells[0].text = str('Wajib:\n\t1. Foto Before & After instalasi/ implementasi dg aplikasi kamera GPS\n\t2. Kabel power perangkat, Kabel LAN, Kabel modem, dll yg sifatnya utk konektivitas (dari dan ke)\n(hapus panduan kalimat miring ketika akan diisi informasi)')
        row_cells[0].width = Cm(17.37)
        
        run0 = row_cells[0].paragraphs[0].runs[0].font
        run0.name = 'Tahoma'
        run0.size = Pt(10)
        run0.italic = True
        # p = document.add_paragraph('')


        document.add_paragraph()
        #Gambar Testing Beban
        ptirib = document.add_table(rows=1, cols=1, style='Table Grid')
        hdr_cells = ptirib.rows[0].cells
        hdr_cells[0].text = 'Gambar Testing Beban'
        hdr_cells[0].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.CENTER
        run0 = hdr_cells[0].paragraphs[0].runs[0]
        run0.bold = True
        run0 = hdr_cells[0].paragraphs[0].runs[0].font
        run0.name = 'Tahoma'
        run0.size = Pt(11)
        hdr_cells[0].width = Cm(17.37)
        row_cells = ptirib.add_row().cells
        row_cells[0].text = str('Wajib:\n\t1. Gambar tampilan layar utuh/ capture layar monitor/ print screen Before & After instalasi/\n\t    implementasi\n\t2. Isi jika instalasi baru/ relokasi/ pergantian perangkat (replacement)\n(hapus panduan kalimat miring ketika akan diisi informasi)')
        # number1 = row_cells[0].paragraphs[0]
        row_cells[0].width = Cm(17.37)
        
       
        # number1.style = document.styles['List Number']
        
        run0 = row_cells[0].paragraphs[0].runs[0].font
        run0.name = 'Tahoma'
        run0.size = Pt(10)
        run0.italic = True 
        document.add_paragraph()
        

        #Gambar Serah Terima Dengan PIC
        ptirib = document.add_table(rows=1, cols=1, style='Table Grid')
        hdr_cells = ptirib.rows[0].cells
        hdr_cells[0].text = 'Gambar Serah Terima Dengan PIC'
        hdr_cells[0].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.CENTER
        run0 = hdr_cells[0].paragraphs[0].runs[0]
        run0.bold = True
        run0 = hdr_cells[0].paragraphs[0].runs[0].font
        run0.name = 'Tahoma'
        run0.size = Pt(11)
        hdr_cells[0].width = Cm(17.37)
        row_cells = ptirib.add_row().cells
        row_cells[0].text = str('Isi jika instalasi baru/ relokasi/ pergantian perangkat (replacement)/ dismantle (cabut layanan)dg aplikasi kamera GPS\n(hapus panduan kalimat miring ketika akan diisi informasi)')
        run0 = row_cells[0].paragraphs[0].runs[0].font
        run0.name = 'Tahoma'
        run0.size = Pt(10)
        run0.italic = True
        row_cells[0].width = Cm(17.37)
        document.add_paragraph('')

        #Catatan Tambahan (Jika ada) :
        ptirib = document.add_table(rows=1, cols=1, style='Table Grid')
        hdr_cells = ptirib.rows[0].cells
        hdr_cells[0].text = 'Catatan Tambahan (Jika ada) :'
        # hdr_cells[0].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.CENTER
        run0 = hdr_cells[0].paragraphs[0].runs[0]
        run0.bold = True
        run0 = hdr_cells[0].paragraphs[0].runs[0].font
        run0.name = 'Tahoma'
        run0.size = Pt(11)
        hdr_cells[0].width = Cm(17.37)
        row_cells = ptirib.add_row().cells
        row_cells[0].text = str('Isi informasi yang ditemukan ketika instalasi baru/ relokasi/ pergantian perangkat (replacement)/ dismantle (cabut layanan). Contoh :\nTidak ada  Rak/ Wadah Perangkat,  Tidak ada  AC,  Tidak ada  UPS\n(hapus panduan kalimat miring ketika akan diisi informasi)')
        run0 = row_cells[0].paragraphs[0].runs[0].font
        run0.name = 'Tahoma'
        run0.size = Pt(10)
        run0.italic = True

        document.add_paragraph('') 

        p = document.add_paragraph('') 
        paragraph = p.add_run('-0-')
        p.alignment = 1 # for left, 1 for center, 2 right, 3 justify ...
        paragraph.font.name = 'Tahoma'
        paragraph.font.size = docx.shared.Pt(11)
    


        

        document.save("output/" + file +".docx")

        
        
        